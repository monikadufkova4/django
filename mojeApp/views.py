from django.shortcuts import render
from django.http import HttpResponse
from mojeApp.models import Topic, Webpage, AccessRecord
# Create your views here.

def index (request):
    webpages_list = AccessRecord.objects.order_by('date')
    date_dict = {'access_records':webpages_list}
    my_dict={'inserts_me': 'Hello, I am from views.py'}
    return render (request, 'mojeApp/index.html', context=my_dict)

def index2 (request):
    return HttpResponse ('Funguje')

def djangoG (request):
    return render (request, 'mojeApp/DjangoGuitar.html')
