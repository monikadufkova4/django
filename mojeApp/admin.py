from django.contrib import admin
from mojeApp.models import AccessRecord, Topic, Webpage, Post


# Register your models here.

admin.site.register (AccessRecord)
admin.site.register (Topic)
admin.site.register (Webpage)
admin.site.register(Post)
